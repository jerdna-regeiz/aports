# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bear
pkgver=3.0.7
pkgrel=0
pkgdesc="Tool which generates a compilation database for clang tooling"
url="https://github.com/rizsotto/Bear"
# ppc64le armv7 armhf: Limited by grpc
# s390x: Test failure <https://github.com/rizsotto/Bear/issues/309>
arch="all !s390x !ppc64le !armv7 !armhf"
license="GPL-3.0-or-later"
makedepends="cmake grpc grpc-dev fmt-dev spdlog-dev sqlite-dev
	nlohmann-json protobuf-dev gtest-dev c-ares-dev"
subpackages="$pkgname-doc"
source="https://github.com/rizsotto/Bear/archive/$pkgver/bear-$pkgver.tar.gz"
builddir="$srcdir/Bear-$pkgver"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_INSTALL_LIBEXECDIR=libexec/bear \
		-DCMAKE_BUILD_TYPE=None .
	make -C build
}

check() {
	cd build
	ctest --verbose --output-on-failure
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="fcde13bd56836c60d41930b38f62f2bba672553167a412bb13cf4f3906ca7fef77be291fe159e64597993807187e75d39eba4fb5c79a3cdd1d2f8c317bac458b  bear-3.0.7.tar.gz"
